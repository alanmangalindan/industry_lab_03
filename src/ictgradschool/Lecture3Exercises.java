package ictgradschool;

public class Lecture3Exercises {

    public static void main(String[] args) {
        Lecture3Exercises ex = new Lecture3Exercises();
//        ex.ex1();
//        ex.ex2();
        ex.ex3();
//        ex.ex4();
    }

    public void ex1() {

        // 1. Write a method that prints the average of all even numbers between 1 and 20 (inclusive).
        int sum = 0;
        int count = 0;
        for (int i = 1; i <= 20; i++) {
            if (i % 2 == 0) {
                sum += i;
                count++;
            }
        }
        System.out.println("The average of all even numbers between 1 and 20 (inclusive) is " + ((double) sum / count));
    }

    public void ex2() {

        // 2. Write a method that prompts the user to type Y or N. If they type anything else, the user should be re-prompted.
        while (true) {
            System.out.print("Enter a response (Y/N): ");
            String response = Keyboard.readInput();
            if (response.toLowerCase().equals("y") || response.toLowerCase().equals("n")) {
                break;
            }
        }
    }

    public void ex3() {

        // 3. Write a method that prompts the user to enter two integers. Then, print all odd numbers between the two given values (inclusive).
        int int1, int2;
        while (true) {
            System.out.print("Enter first integer: ");
            try {
                int1 = Integer.parseInt(Keyboard.readInput());
                break;
            } catch (NumberFormatException e) {
                System.out.println("Please enter an integer.");
            }
        }

        while (true) {
            System.out.print("Enter second integer: ");
            try {
                int2 = Integer.parseInt(Keyboard.readInput());
                break;
            } catch (NumberFormatException e) {
                System.out.println("Please enter an integer.");
            }
        }

        System.out.println("All odd numbers between " + int1 + " and " + int2 + " are:");
        for (int i = Math.min(int1, int2); i <= Math.max(int1, int2); i++) {
            if (i % 2 != 0) {
                System.out.print(i + "  ");
            }
        }
        System.out.println();
    }

    public void ex4() {

        // 4. Write a method that prints all the factorials from 1! To 10! (where n! = n * (n-1) * (n-2) * … * 2 * 1).
        for (int i = 1; i <= 10; i++) {
            int result = 1;
            for (int j = i; j > 0; j--) {
                if (j <= 1) {
                    result *= 1;
                } else {
                    result *= j;
                }
            }
            System.out.println(i + "! = " + result);
        }

    }

}
